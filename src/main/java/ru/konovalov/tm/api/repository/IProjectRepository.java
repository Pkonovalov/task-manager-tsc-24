package ru.konovalov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.konovalov.tm.api.IOwnerRepository;
import ru.konovalov.tm.model.Project;

public interface IProjectRepository extends IOwnerRepository<Project> {

    boolean existsByName(@NotNull String userId, @NotNull String name);

    @Nullable
    Project findOneByIndex(@NotNull String userId, @NotNull Integer index);

    @Nullable
    Project findOneByName(@NotNull String userId, @NotNull String name);

    @Nullable
    Project removeOneByIndex(@NotNull String userId, @NotNull Integer index);

    @Nullable
    Project removeOneByName(@NotNull String userId, @NotNull String name);

    @Nullable
    String getIdByName(@NotNull String userId, @NotNull String name);

}
