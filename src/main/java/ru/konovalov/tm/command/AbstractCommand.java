package ru.konovalov.tm.command;

import ru.konovalov.tm.api.service.ServiceLocator;
import ru.konovalov.tm.enumerated.Role;

public abstract class AbstractCommand {

    protected ServiceLocator serviceLocator;

    public void setServiceLocator(ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    public abstract String arg();

    public abstract String name();

    public abstract String description();

    public abstract void execute();

    public Role[] roles() {
        return null;
    }

}

