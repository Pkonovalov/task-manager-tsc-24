package ru.konovalov.tm.command.task;

import ru.konovalov.tm.model.User;
import ru.konovalov.tm.util.TerminalUtil;

public final class TaskByIndexFinishCommand extends AbstractTaskCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "task-finish-by-index";
    }

    @Override
    public String description() {
        return "Finish task by index";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[START TASK]");
        System.out.println("ENTER INDEX:");
        serviceLocator.getTaskService().startTaskByIndex(userId, TerminalUtil.nextNumber() - 1);
    }

}
