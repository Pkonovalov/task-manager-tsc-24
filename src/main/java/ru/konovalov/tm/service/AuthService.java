package ru.konovalov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.konovalov.tm.api.service.IAuthService;
import ru.konovalov.tm.api.service.IUserService;
import ru.konovalov.tm.enumerated.Role;
import ru.konovalov.tm.exeption.empty.EmptyIdException;
import ru.konovalov.tm.exeption.empty.EmptyLoginException;
import ru.konovalov.tm.exeption.empty.EmptyPasswordException;
import ru.konovalov.tm.exeption.system.AccessDeniedException;
import ru.konovalov.tm.exeption.user.NotLoggedInException;
import ru.konovalov.tm.model.User;
import ru.konovalov.tm.util.HashUtil;

import static ru.konovalov.tm.util.ValidationUtil.isEmpty;

public class AuthService implements IAuthService {

    @NotNull
    private final IUserService userService;

    @Nullable
    private String userId;

    public AuthService(@NotNull final IUserService userService) {
        this.userService = userService;
    }

    @NotNull
    @Override
    public String getUserId() {
        if (userId == null) throw new AccessDeniedException();
        return userId;
    }

    @NotNull
    @Override
    public User getUser() {
        if (isEmpty(userId)) throw new NotLoggedInException();
        @Nullable final String userId = getUserId();
        @Nullable final User user = userService.findById(userId);
        if (user == null) throw new EmptyIdException();
        return user;
    }

    public void checkRoles(@Nullable final Role... roles) {
        if (roles == null || roles.length == 0) return;
        @Nullable final User user = getUser();
        if (user == null) throw new AccessDeniedException();
        @Nullable final Role role = user.getRole();
        if (role == null) throw new AccessDeniedException();
        for (final Role item : roles) {
            if (item.equals(role)) return;
        }
        throw new AccessDeniedException();
    }

    @Override
    public boolean isAuth() {
        return userId == null;
    }

    @Override
    public void logout() {
        if (isEmpty(userId)) throw new NotLoggedInException();
        userId = null;
    }

    @Override
    public void login(@Nullable final String login, @Nullable final String password) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        @Nullable final User user = userService.findByLogin(login);
        if (user == null) throw new AccessDeniedException();
        @Nullable final String hash = HashUtil.salt(password);
        if (hash == null) throw new AccessDeniedException();
        if (!hash.equals(user.getPasswordHash()) || user.isLocked()) throw new AccessDeniedException();
        userId = user.getId();
    }

    @Override
    public void registry(@Nullable final String login, @Nullable final String password, final String email) {
        userService.add(login, password, email);
    }
}

