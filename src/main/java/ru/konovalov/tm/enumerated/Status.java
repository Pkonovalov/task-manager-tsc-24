package ru.konovalov.tm.enumerated;

import ru.konovalov.tm.exeption.entity.StatusNotFoundException;

import static ru.konovalov.tm.util.ValidationUtil.checkStatus;

public enum Status {

    NOT_STARTED("Not started"),
    IN_PROGRESS("In progress"),
    COMPLETE("Complete");

    private String displayName;

    Status(String displayName) {
        this.displayName = displayName;
    }

    public static void main(String[] args) {
        System.out.println(COMPLETE);
    }

    public static Status getStatus(String s) {
        s = s.toUpperCase();
        if (!checkStatus(s)) throw new StatusNotFoundException();
        return Status.valueOf(s);
    }

    public String getDisplayName() {
        return displayName;
    }

}




